<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books_categories', function (Blueprint $table) {

            $table->integer('book_id')->unsigned()->length(10);
            $table->integer('category_id')->unsigned()->length(10);
            $table->timestamps();

            echo "adding foreign key Book Category \n";

            $table->foreign('book_id','ref_book')->references('id')->on('books')->onDelete('cascade');
            $table->foreign('category_id','ref_category')->references('id')->on('categories')->onDelete('cascade');
            $table->primary(array('book_id', 'category_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
