<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>BOOKS APP</title>
    <link rel="stylesheet" href="../../css/app.css">

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-2">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><strong>BOOKS</strong></a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/register"> REGISTER</a></li>
                @if(Auth::Check())

                    <li><a href="/books"> BOOKS</a></li>
                    <li class="dropdown user-control">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <strong>Welcome, </strong> {{ Auth::User()->name }}
                            <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/books">Books</a></li>
                            <li class="divider"></li>
                            <li><a href="/logout" class="logout"><i class="fa fa-power-off" aria-hidden="true"></i> LOGOUT</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/login" class="login"><i class="fa fa-power-off" aria-hidden="true"></i>&nbsp; LOGIN</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>
<div id="app" class="container-fluid">
    @yield('content')
</div>
</body>
<script type="application/javascript" src="../../js/app.js"></script>
</html>