@extends('master')

@section('content')
    <h3>Welcome to Books APP</h3>


    @if(Auth::check())

            To visit you books click in follow link: <a href=/books class="btn btn-primary">BOOKS</a>

    @else
            To visit you books please login: <a href=/login class="btn btn-primary">LOGIN</a>

    @endif
@endsection