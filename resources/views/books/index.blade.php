@extends('master')

@section('content')
    <index-book inline-template>
        <div>
            <div class="col-xs-12">
                <div class="legend-top"><span>MY BOOKS</span>
                    <a class="btn btn-primary fr" href="books/edit/-1"><i class="fa fa-plus" aria-hidden="true"></i> ADD
                        NEW
                        BOOK</a>
                </div>
                <div class="legend-sub-top">

                    <div class="form-group">
                        @foreach($categories as $category)
                            <div class="checkbox-inline">
                                <input v-on:click="checkCategory($event)" type="checkbox"
                                       value="{{ $category->id }}">{{ $category->name }}</input>
                            </div>
                        @endforeach
                    </div>
                    <a class="btn btn-primary fr" v-on:click="search()"><i class="fa fa-search" aria-hidden="true"></i>
                        SEARCH</a>
                </div>
            </div>
            <div class="col-xs-12 books no-padding">

                <div v-show="removeControl" class="alert alert-dismissible alert-warning margin-15">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Are you sure about to delete this BOOK ?</strong>
                    <div class="tar">
                        <button type="button" v-on:click="removeControl = false" class="btn btn-default margin-right-10">
                            CANCEL
                        </button>
                        <button type="button" v-on:click="RemoveConfirm()" class="btn btn-primary">
                            CONFIRM
                        </button>
                    </div>
                </div>

                <div v-for="book in books" class="col-xs-12 col-sm-4 col-md-3">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title inline-block">BOOK @{{ book.id }}</h3>

                            <button type="button" v-on:click="Remove(book)" class="btn btn-sm btn-danger fr margin-left-5">
                                <i class="fa fa-trash" aria-hidden="true"></i>
                            </button>

                            <a v-bind:href="'/books/edit/' + book.id" type="button" class="btn btn-sm btn-info fr">
                                <i class="fa fa-pencil" aria-hidden="true"></i>
                            </a>

                        </div>
                        <div class="panel-body">
                            <div><strong>Title: </strong>@{{ book.title }}</div>
                            <div><strong>Year: </strong>@{{ book.year }}</div>
                            <div>
                                <span class="label label-default margin-right-10" v-for="category in book.categories">
                                  @{{ category.name }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </index-book>
@endsection