@extends('master')

@section('content')
    <edit-book inline-template :editbook="{{ $book->toJson() }}">



        <form v-on:submit="OnSubmit" method="post" role="form">
            <div v-show="state" class="alert alert-dismissible alert-success">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <strong>Well done!</strong> Book successfully saved !.
            </div>
            <legend>Form Title</legend>

            <div class="form-group col-xs-12 col-sm-8 col-md-6 no-padding"  v-bind:class="fields.book.title.class">
                <label for="">Title</label>
                <input type="text" v-model="book.title" class="form-control" name="" id="" placeholder="Title of the book">
                <span class="help-block" v-show="!fields.book.title.state" v-text="fields.book.title.msg"></span>
            </div>
            <div class="form-group col-xs-12 col-sm-4  col-md-6 no-padding padding-left-15"  v-bind:class="fields.book.year.class">
                <label for="">Year</label>
                <input type="number" v-model="book.year" class="form-control" name="" id="" placeholder="Year ex:. 2015">
                <span class="help-block" v-show="!fields.book.year.state" v-text="fields.book.year.msg"></span>
            </div>
            <div id="categories" class="form-group col-xs-12" v-bind:class="fields.categories.class" >
                <label for="" class="w100p">Categories</label>
                @foreach($categories as $category)
                    <div class="checkbox-inline">
                        <input type="checkbox" v-on:click="checkCategory($event)" value="{{ $category->id }}">{{ $category->name }}</input>
                    </div>
                @endforeach
                <span class="help-block" v-show="!fields.categories.state" v-text="fields.categories.msg"></span>
            </div>

            <div class="form-group tar">
                <a href="/books" type="submit" class="btn btn-danger">CLOSE</a>
                <button type="submit" class="btn btn-primary">SAVE</button>
            </div>

        </form>
    </edit-book>
@endsection