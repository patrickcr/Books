@extends('master')

@section('content')
    <register inline-template>
        <div class="col-xs-12 col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">

            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">CREATE AN ACCOUNT</h3>
                </div>
                <div class="panel-body">
                    <form v-on:submit="submit"  role="form">
                        <legend>REGISTER FORM</legend>

                        <div class="form-group" v-bind:class="fields.name.class">
                            <label for="">Your Name</label>
                            <input type="text" v-model="user.name" class="form-control" name="" id="" placeholder="Your Name">
                            <span class="help-block" v-show="!fields.name.state" v-text="fields.name.msg"></span>
                        </div>

                        <div class="form-group" v-bind:class="fields.email.class">
                            <label for="">Email</label>
                            <input type="email" v-model="user.email" class="form-control" name="" id="" placeholder="Your Email">
                            <span class="help-block" v-show="!fields.email.state" v-text="fields.email.msg"></span>
                        </div>

                        <div class="form-group" v-bind:class="fields.password.class">
                            <label for="">Password</label>
                            <input type="password" v-model="user.password" class="form-control" name="" id="" placeholder="Your Password">
                            <span class="help-block" v-show="!fields.password.state" v-text="fields.password.msg"></span>
                        </div>
                        <div class="form-group tar">
                            <button type="submit" class="btn btn-primary">REGISTER</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </register>
@endsection