<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Book extends Model
{
    protected $table = "books";
    protected $fillable = [];
    protected $guarded = [
        'id',
    ];

    public function Categories()
    {

        return $this->belongsToMany('App\Category', 'books_categories');
    }

    public function scopeSearch($query, $request)
    {
        $categories = $request->all();

        if (count($categories))

            $books = $query->with('Categories')->where('user_id', Auth::User()->id)
                ->whereHas('categories', function ($query) use ($categories) {
                    $query->whereIn('id', $categories);
                })->get();
        else
            $books = $query->with('Categories')->where('user_id', Auth::User()->id)->get();

        return $books;
    }
}
