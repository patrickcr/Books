<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = "categories";
    protected $fillable = [];
    protected $guarded = [
        'id',
    ];

    protected function scopeAll($query)
    {
        return $query->all()->get();
    }

    public function Book(){

        return $this->belongsToMany('App\Book', 'books_categories');
    }
}
