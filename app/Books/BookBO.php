<?php

namespace App\Books;

use App\Book;
use App\FormResult;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class BookBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();

    }


    public function messages()
    {

        return [
            'book.title.required' => "The field title is necessary",
            'book.year.required' => 'The field year is necessary',
            'categories.required' => "At leat one category is required",
        ];
    }

    public function validateRequest()
    {


        $this->validate($this->request, [
            'book.title' => 'required',
            'book.year' => 'required',
            'categories' => 'required'
        ], $this->messages());


        return $this;

    }




    public function saveDB()
    {


        $result = new FormResult();

        try {

            if ($this->request->book['id'] > 0)
                $book = Book::find($this->request->book['id']);
            else
                $book = new Book();

            $book->fill($this->request->book);
            $book->user_id = Auth::User()->id;
            $book->save();

            $book->Categories()->sync($this->request->categories);
            $result->data = $book;

        } catch (Exception $ex) {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try {
            $book = Book::find($id);
            if ($book != null)
                $book->delete();
        } catch (Exception $ex) {
            $result->result = false;
        }
        return $result->toJson();

    }

}

?>