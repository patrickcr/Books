<?php

namespace App\Users;

use App\FormResult;
use App\User;
use App\Validation\ValidateRequestsForApp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class UserBO
{
    use ValidateRequestsForApp;


    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function save()
    {

        return $this->validateRequest()->saveDB();

    }

    public function register()
    {

        return $this->validateRequest()->saveRegister();

    }


    public function login()
    {

        return $this->validateLoginRequest()->Authenticate();

    }

    public function logout()
    {
        Auth::logout();

        return redirect()->route('home');
    }

    public function messages()
    {

        return [
            'name.required' => "The field name is necessary",
            'email.required' => 'The field email is necessary',
            'email.unique' => "This email already exists",
            'password.required' => "The field password is necessary",
        ];
    }

    public function messages2()
    {

        return [
            'email.required' => 'The field email is necessary',
            'email.exists' => "We can't find your email in our database",
            'password.required' => "The field password is necessary",
        ];
    }

    public function validateRequest()
    {


        $this->validate($this->request, [
            'name' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required'
        ], $this->messages());


        return $this;

    }


    public function validateLoginRequest()
    {


        $this->validate($this->request, [
            'email' => 'required|exists:users',
            'password' => 'required'
        ], $this->messages2());


        return $this;
    }

    /**
     * @return string
     */
    public function Authenticate()
    {
        $result = new FormResult();


        if (Auth::attempt(['email' => $this->request->email, 'password' => $this->request->password])) {

            // Authentication passed...
            $result->result = true;
        } else {
            $result->data = "Please verify your credentials !";
            $result->result = false;
        }

        return $result->toJson();
    }

    public function saveRegister()
    {

        $result = new FormResult();

        try {


            $user = new User();

            $user->fill($this->request->all());

            $user->password = Hash::make($this->request->password);

            $user->save();

            Auth::login($user);


        } catch (Exception $ex) {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function saveDB()
    {


        $result = new FormResult();

        try {

            if ($this->request->id > 0)
                $user = User::find($this->request->id);
            else
                $user = new User();

            $user->fill($this->request->all());

            $user->save();

            if (!empty($this->request->password)) {
                $user->password = Hash::make($this->request->password);
                $user->save();

            }
        } catch (Exception $ex) {

            $result->result = false;
        }

        return $result->toJson();

    }

    public function delete($id)
    {
        $result = new FormResult();
        try {
            $location = User::find($id);
            if ($location != null)
                $location->delete();
        } catch (Exception $ex) {
            $result->result = false;
        }
        return $result;

    }

}

?>