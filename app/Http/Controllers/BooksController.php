<?php

namespace App\Http\Controllers;

use App\Book;
use App\Books\BookBO;
use App\Category;
use Illuminate\Http\Request;


class BooksController extends Controller
{

    public function all(Request $request)
    {

        return Book::Search($request);
    }

    public function view()
    {
        $categories = Category::All();

        return view('books.index')->with('categories', $categories);
    }

    public function edit($id)
    {

        if($id > 0)
            $book = Book::with('Categories')->find($id);
        else
            $book = new Book();

        $categories = Category::All();
        return view('books.edit')->with('categories', $categories)->with("book", $book);
    }

    public function save(BookBO $bookBO)
    {
        return $bookBO->save();
    }

    public function remove($id, BookBO $bookBO)
    {
        return $bookBO->delete($id);
    }

}
