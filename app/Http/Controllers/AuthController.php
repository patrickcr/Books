<?php

namespace App\Http\Controllers;

use App\Users\UserBO;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register_view()
    {
        return view('register');
    }

    public function login_view()
    {
        return view('login');
    }

    public function register(UserBO $usersBO)
    {

        return $usersBO->register();
    }

    public function login(UserBO $usersBO)
    {

        return $usersBO->login();
    }

    public function logout(UserBO $userBO)
    {

        return $userBO->logout();
    }


}
