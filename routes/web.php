<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');
Route::get('register','AuthController@register_view');
Route::get('login','AuthController@login_view');
Route::put('register','AuthController@register');
Route::get('authenticate','AuthController@login');
Route::get('logout','AuthController@logout');

Route::get('books','BooksController@view')->middleware('auth');
Route::get('books/edit/{id}','BooksController@edit')->middleware('auth');
Route::get('books/save','BooksController@save')->middleware('auth');
Route::get('books/all','BooksController@all')->middleware('auth');
Route::get('books/remove/{id}','BooksController@remove')->middleware('auth');