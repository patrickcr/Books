<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class RouteTest extends TestCase
{
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testHome()
    {
        $this->visit('/')
             ->see('Books APP');
    }

    public function testLogin()
    {
        $this->visit('/login')
            ->see('LOGIN FORM');
    }

    public function testRegister()
    {
        $this->visit('/register')
            ->see('Register FORM');
    }

}
