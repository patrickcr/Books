<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class CategoriesTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testComedie()
    {
        $this->seeInDatabase('categories', [
            'name' => 'COMEDIE'
        ]);
    }

    public function testAddCategory()
    {
        $category = factory(App\Category::class)->make();

        $category->save();

        $this->seeInDatabase('categories', [
            'name' => 'HISTORY'
        ]);
    }

}
